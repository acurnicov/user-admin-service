package edu.endava.user.service.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.sql.Date;

@Data
@Entity
@Table(name = "users")
@AllArgsConstructor
public class User {
    public User() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_generator")
    @SequenceGenerator(name = "user_id_generator", sequenceName = "users_increment", allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;

    @Size(min = 3, max = 30, message = "Please, fill in a value between 3 and 30 characters")
    @NotEmpty(message = "Please, fill in your first name")
    @Column(name = "first_name")
    private String firstName;

    @Size(min = 3, max = 30, message = "Please, fill in a value between 3 and 30 characters")
    @NotEmpty(message = "Please, fill in your last name")
    @Column(name = "last_name")
    private String lastName;

    @Pattern(regexp = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", message = "Please, enter a valid email")
    @Column(name = "email")
    private String email;

    @Size(min = 1, max = 2, message = "Please, fill in a value between 1 and 2 characters")
    @Column(name = "role")
    private String role;

    @Size(min = 8, max = 32, message = "Please, fill in a value between 8 and 32 characters")
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$", message = "Please, enter a valid with at least one letter(lowercase and uppercase), one number and 1 special character")
    @Column(name = "password")
    private String password;

    @Column(name = "register_date")
    private Date registerDate;

    @Column(name = "bith_date")
    private Date birthDate;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JsonIgnoreProperties("users")
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    private Group group;

    @Override
    public String toString() {
        return this.id + " " + this.firstName + " " + this.group;
    }
}
