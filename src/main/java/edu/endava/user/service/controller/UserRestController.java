package edu.endava.user.service.controller;

import edu.endava.user.service.entity.User;
import edu.endava.user.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/users", produces = "application/json")
public class UserRestController {
    private final UserRepository userRepository;

    @Autowired
    public UserRestController(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<User> find(@PathVariable final int id) {
        return new ResponseEntity<>(userRepository.findById(id).orElse(new User()), HttpStatus.OK);
    }

    @GetMapping(path = "/")
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @PostMapping(path = "/")
    public User add(@RequestBody final User user) {
        return userRepository.save(user);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<User> delete(@PathVariable("id") final int id) {
        try {
            userRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch(Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "/")
    public User update(@RequestBody final User user) {
        return userRepository.save(user);
    }
}