package edu.endava.user.service.controller;

import javax.validation.Valid;

import edu.endava.user.service.entity.User;
import edu.endava.user.service.repository.GroupRepository;
import edu.endava.user.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
@RequestMapping("/users")
public class UserController {
    private final UserRepository userRepository;
    private final GroupRepository groupRepository;

    @Autowired
    public UserController(final UserRepository userRepository, final GroupRepository groupRepository) {
        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
    }

    @GetMapping("/")
    public ModelAndView findAll() {
        final ModelAndView model = new ModelAndView("index");
        model.addObject("users", userRepository.findAll());
        return model;
    }

    @PostMapping("/")
    public ModelAndView save(@Valid final User user, final BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("user/add");
        }

        final ModelAndView model = new ModelAndView("index");
        userRepository.save(user);
        model.addObject("success", "User signed up successfully.");
        model.addObject("users", userRepository.findAll());
        return model;
    }

    @GetMapping("add")
    public ModelAndView showSignUpForm(final User user) {
        final ModelAndView model = new ModelAndView("user/add");
        model.addObject("groups", groupRepository.findAll());
        model.addObject("currentDate", new Date());
        return model;
    }

    @GetMapping("edit/{id}")
    public ModelAndView showUpdateForm(@PathVariable("id") final int id) {
        final ModelAndView model = new ModelAndView("user/update");
        final User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        model.addObject("user", user);
        model.addObject("groups", groupRepository.findAll());
        return model;
    }

    @PostMapping("/update/{id}")
    public ModelAndView updateStudent(@PathVariable("id") final int id, @Valid final User user, final BindingResult result) {
        if (result.hasErrors()) {
            final ModelAndView model = new ModelAndView("user/update");
            user.setId(id);
            return model;
        }

        final ModelAndView model = new ModelAndView("index");
        userRepository.save(user);
        model.addObject("users", userRepository.findAll());
        return model;
    }

    @GetMapping("delete/{id}")
    public ModelAndView deleteStudent(@PathVariable("id") final int id) {
        final ModelAndView model = new ModelAndView("index");
        final User user = userRepository.findById(id).orElse(new User());
        userRepository.delete(user);
        model.addObject("users", userRepository.findAll());
        return model;
    }
}