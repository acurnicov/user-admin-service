package edu.endava.user.service.config;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class Config {
    @Bean
    public DataSource dataSource() {
        final DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("oracle.jdbc.driver.OracleDriver");
        dataSourceBuilder.url("jdbc:oracle:thin:@//localhost:1521/orclpdb");
        dataSourceBuilder.username("hibernate_homework");
        dataSourceBuilder.password("artursqlpassword123");
        return dataSourceBuilder.build();
    }
}
