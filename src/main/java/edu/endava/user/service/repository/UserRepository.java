package edu.endava.user.service.repository;

import edu.endava.user.service.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("from User u where upper(u.firstName) = upper(:firstName)")
    List<User> findByFirstName(@Param("firstName") String firstName);

    @Query("from User u where upper(u.role) = upper(:role)")
    List<User> findUsersByRole(@Param("role") String role);

    List<User> findAllByLastName(final String lastName);
}
